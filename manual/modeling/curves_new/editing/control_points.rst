
**************
Control Points
**************

.. _bpy.ops.curves.extrude_move:

Extrude Curve and Move
======================

.. reference::

   :Mode:      Edit Mode
   :Shortcut:  :kbd:`E`

Extrudes points by duplicating the selected points, which then can be moved,
and connecting those points back to the original curve creating a continuous curve.


.. _modeling-curves-tilt:

Tilt
====

.. reference::

   :Mode:      Edit Mode
   :Tool:      :menuselection:`Toolbar --> Tilt`
   :Shortcut:  :kbd:`Ctrl-T`

This setting controls how the normals twist around each control point.
The tilt will be interpolated from point to point (you can check it with the normals).


.. _bpy.ops.curves.tilt_clear:

Clear Tilt
==========

.. reference::

   :Mode:      Edit Mode
   :Shortcut:  :kbd:`Alt-T`

You can also reset the tilt to its default value (i.e. perpendicular to the original curve plane).

