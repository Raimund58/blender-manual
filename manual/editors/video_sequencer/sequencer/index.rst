
*********
Sequencer
*********

The Sequencer view type shows a timeline and allows placing and editing strips.

.. figure:: /images/editors_vse_sequencer.svg
   :alt: Sequencer

   The Sequencer view and its components.

.. toctree::
   :maxdepth: 2

   introduction.rst
   channels.rst
   navigating.rst
   toolbar/index.rst
   sidebar/index.rst
   display.rst
