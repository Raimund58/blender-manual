
*********
Todo List
*********

This page provides a list of changes that need to be made to the manual.
This is a great place for new contributors to start but also check
the `documentation workboard <https://projects.blender.org/blender/documentation/projects>`__.

.. todolist::
